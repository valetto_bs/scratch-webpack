import React, { Component } from 'react'
import { Link, NavLink } from 'react-router-dom'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars, faCaretDown } from '@fortawesome/free-solid-svg-icons'

class Sidebar extends Component {

  constructor(props) {
        super(props);
        library.add([faBars, faCaretDown])
  }

  toggleClass(e) {
    e.currentTarget.closest(".dropdown").classList.toggle("dropdown--hide");
  }

  render () {
    return (
    	<div className="sidebar sidebar--fixed">
        <NavLink exact className="nav-link" activeClassName="active" to="/">Dashboard</NavLink>
        <NavLink className="nav-link" activeClassName="active" to="/about">About</NavLink>
        <div className="dropdown dropdown--hide">
          <div className="nav-link" to="/" onClick={(e) => this.toggleClass(e)} >Dropdown <FontAwesomeIcon icon="caret-down" /></div>
          <ul className="dropdown-menu">
            <li><a className="dropdown-item" href="#">Me</a></li>
            <li><a className="dropdown-item" href="#">Gaming</a></li>
            <li><a className="dropdown-item" href="#">Sport</a></li>
            <li><a className="dropdown-item" href="#">Web Design</a></li>
            <li><NavLink className="dropdown-item" activeClassName="active" to="/about">About</NavLink></li>
          </ul>
        </div>
        <div className="dropdown dropdown--hide" >
          <div className="nav-link" to="/" onClick={(e) => this.toggleClass(e)}>Dropdown <FontAwesomeIcon icon="caret-down" /></div>
          <ul className="dropdown-menu">
            <li><a className="dropdown-item" href="#">Me</a></li>
            <li><a className="dropdown-item" href="#">Gaming</a></li>
            <li><a className="dropdown-item" href="#">Sport</a></li>
            <li><a className="dropdown-item" href="#">Web Design</a></li>
            <li><a className="dropdown-item" href="#">Web Development</a></li>
          </ul>
        </div>
    	</div>
    )
  }
}

export default Sidebar;