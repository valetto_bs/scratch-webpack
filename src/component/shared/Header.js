import React, { Component } from 'react'
import { Link, NavLink } from 'react-router-dom'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars, faCaretDown } from '@fortawesome/free-solid-svg-icons'

class Header extends Component {

	constructor(props) {
        super(props);
        library.add([faBars, faCaretDown])
    }

    do_logout(e) {
    	e.preventDefault();
		localStorage.clear();
		window.location.href = "/";
    	// localStorage.removeItem('token');
    	// localStorage.removeItem('refreshToken');
    }

  	render () {
    	return (
    		<div className="navbar navbar--fixed">
				<div className="container container--fluid">
    				<div className="navbar--content">
	    				<div className="left">
				    		<span className="nav-link menu-toggle" onClick={this.props.handleClick} ><FontAwesomeIcon icon="bars" /></span>
			  				<Link className="nav-link logo" to="/">BINEX</Link>
		  				</div>
	    				<div className="right">
		  					<NavLink className="nav-link" activeClassName="active" to="/about">About</NavLink>
			  				<div className="dropdown">
			  					<span className="nav-link" to="/">User <FontAwesomeIcon icon="caret-down" /></span>
								<ul className="dropdown-menu">
							{/*		<li><a className="dropdown-item" href="#">Me</a></li>
									<li><a className="dropdown-item" href="#">Gaming</a></li>
									<li><a className="dropdown-item" href="#">Sport</a></li>
									<li><a className="dropdown-item" href="#">Web Design</a></li>*/}
									<li><a className="dropdown-item" href="#">Settings</a></li>
									<li><a className="dropdown-item" href="#" onClick={(e) => (this.do_logout(e))}>Logout</a></li>
								</ul>
			  				</div>
		  				</div>
		  			</div>
	        	</div>
        	</div>
   		)
  	}
}

export default Header