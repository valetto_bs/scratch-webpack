import React, { Component } from 'react'
import Loader from '../shared/Loader'

class Home extends Component {

  constructor() {
      super();
      this.state = {
          showLoader: true,
          result: []
      };

  }

	componentDidMount() {
		document.title = "Home Title";
		this.load_data();
	}

    load_data() {
    	let prevData;
    	const newData = [];

		this.setState({
		  showLoader: true
		});    	

		fetch('https://jsonplaceholder.typicode.com/photos')
		.then(response => response.json())
		.then( json => {
			this.setState({
		  		showLoader: false,
		  		result: json
			});
		});

    }

	render() {
		const isLoggedIn = this.state.showLoader;
		let data;

		if (isLoggedIn) {
			this.state.result.map( (element, index) => {
				return (
					<div key={index}>
						<div>{element.albumId}</div>
						<div>{element.title}</div>
					</div> 
				)
			});
		}

		return (
			<div>
				<h1>Home Page</h1>
				<p>The spectacle before us was indeed sublime. Apparently we had reached a great height in the atmosphere, for the sky was a dead black, and the stars had ceased to twinkle. By the same illusion which lifts the horizon of the sea to the level of the spectator on a hillside, the sable cloud beneath was dished out, and the car seemed to float in the middle of an immense dark sphere, whose upper half was strewn with silver. Looking down into the dark gulf below, I could see a ruddy light streaming through a rift in the clouds.</p>
			
				 {isLoggedIn ? (
			        <div className="text-center"><Loader /></div>
			      ) : (
			        this.state.result.map( (element, index) => {
						return (
							<div key={index}>
								<div>{element.albumId}</div>
								<div>{element.title}</div>
							</div> 
						)
					})
			      )}
			</div>
		)
	}
}

export default Home