import React, { Component } from 'react'
import { BrowserRouter } from 'react-router-dom'
import Master from './container/Master'
import Guest from './container/Guest'
import ReactDOM from 'react-dom'
import './app.scss'

class App extends Component {

  render () {
    return (
		<BrowserRouter>
		{localStorage.getItem('token') ? <Master /> : <Guest />}
		</BrowserRouter>
    )
  }
}

ReactDOM.render(<App />, document.getElementById('app'));