export let  CallApi = (url, method = "GET", data = {}) => {
	console.log('CallApi Called');

	return new Promise((resolve, reject) => {

		let objHeaders = {
			'Content-Type': 'application/json',
		}

		let token = localStorage.getItem('token')
		if (token) {
			objHeaders.Authorization = 'Bearer ' + token;
		}

		let headers = objHeaders;

		let settings = {
			method,
			headers
		};

		if (method === "POST" || method === "PUT") {
			settings.body = JSON.stringify(data);
		}

		fetch(url, settings)
		.then(response => {
			if (response.status === 429) {
				window.location.href = "/error";
			}
			if (response.ok) {
				return response.json();
			}
			resolve(response.statusText);
		})
		.then((response) => {

			if(response.refresh) {
		    let url = 'http://192.168.2.106:3000/auth/refresh_token';
				let refreshToken = localStorage.getItem('refreshToken')
				let email = localStorage.getItem('email')

		    let data = {};
		    data.email = 'valetto.pereira@bitstreet.co.in';
		    data.refreshToken = refreshToken;

				CallApi(url, "POST",  data)
			}

			if (response.code === 429) {
				console.log('error');
				// window.location.href = "/error";
			}
			if (response.code === 55 || response.code === 454) {
				console.log('Logout');
				// logout();
			}
			resolve(response);
		})
		.catch((error) => {
			console.log("error", error)
			reject(error);
		});

	});

};	