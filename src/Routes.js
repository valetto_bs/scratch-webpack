import React, { Component } from 'react'
import { Route, Switch } from 'react-router-dom'
import home from './component/pages/Home'
import about from './component/pages/About'

class Routes extends Component {
  render () {
    return (
        <Switch>
          <Route exact path='/' component={home} />
          <Route path="/about" component={about} />
        </Switch>
    )
  }
}

export default Routes;