import React, { Component } from 'react'
import Header from '../component/shared/Header'
import Sidebar from '../component/shared/Sidebar'
import Routes from '../Routes'

class Master extends Component {

  constructor() {
      super();
      this.state = {
          showSidebar: true
      };
      this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
      this.setState({
          showSidebar: !this.state.showSidebar
      });
  }

  render () {
    return (
      <div className="dark-theme">
        <Header
          handleClick={this.handleClick} 
          showSidebar={this.state.showSidebar} 
        />
        <div className="container container--fluid content">
          <div className="row">
            { this.state.showSidebar && <Sidebar /> }
            <div className="col"><Routes /></div>
          </div>
        </div>
      </div>
    )
  }
}

export default Master;