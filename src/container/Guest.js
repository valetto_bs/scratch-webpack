import React, { Component } from 'react'
import Routes from '../Routes'
import {CallApi} from '../service/Service'

class Guest extends Component {

  constructor() {
      super();
      // CallApi('http://dummy.restapiexample.com/api/v1/employees').then((result) => {
      //   let responseJson = result;
      //   console.log(responseJson)
      // });
  }

  componentDidMount() {
    document.title = "Login";
  }

  do_login() {
    console.log('do_login')
    let url = 'http://192.168.2.106:3000/auth/do_login';
    let data = {
      username: 'valetto.pereira@bitstreet.co.in',
      password: 'val@123'
    };

    CallApi(url, "POST", data).then(response => {
        if(response.success) {
          console.log('login success');
          localStorage.setItem('token',response.result.token);
          localStorage.setItem('refreshToken',response.result.refresh_token);
          console.log('token :', localStorage.getItem('token'));
          console.log('refreshToken :', localStorage.getItem('refreshToken'));
          // history.push('/');
          window.location.href = "/";
        }
        else {
          alert(response.message);
        }
    }).catch(error => console.error('Error:', error));

    // fetch(url, {
    //   method: 'POST', // or 'PUT'
    //   body: JSON.stringify(data), // data can be `string` or {object}!
    //   headers:{
    //     'Content-Type': 'application/json'
    //   }
    // }).then(res => res.json())
    // .then(
    //   response => {
    //     console.log('Success:', JSON.stringify(response))       

    //     if(response.success) {
    //       console.log('login success');
    //       localStorage.setItem('token',response.result.token);
    //       localStorage.setItem('refreshToken',response.result.refresh_token);
    //       console.log('token :', localStorage.getItem('token'));
    //       console.log('refreshToken :', localStorage.getItem('refreshToken'));
    //       // history.push('/');
    //       window.location.href = "/";
    //     }
    //     else {
    //       alert(response.message);
    //     }
    //   }
    // )
    // .catch(error => console.error('Error:', error));
  }

  render () {
    return (
      <div className="dark-theme">
        <div className="container container--fluid content">
          <div className="row">
            <div className="col">
              <div className="guest">
                <div>LOGIN PAGE</div>
                <button onClick={this.do_login}>LOGIN </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Guest;