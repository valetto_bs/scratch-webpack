const path = require('path')
const express = require('express')
const app = express();
const publicPath = path.join(__dirname, '..', 'public');
const port = process.env.PORT || 3000;

app.use('/assets', express.static(path.join(publicPath, 'dist')));
app.use('/favicon', express.static(path.join(publicPath, 'images', 'favicon')));

app.get('*', (req, res) => {
    res.sendFile(path.join(publicPath, 'index.html'));
});

app.listen(port, () => {
    console.log(`App listening to ${port}....`)
    console.log('Press Ctrl+C to quit.')
});