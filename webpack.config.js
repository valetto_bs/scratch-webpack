const webpack = require('webpack');
const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const Dotenv = require('dotenv-webpack');

const config = {
	mode: "development",
	entry: './src/app.js',
	devtool: 'source-map',
	output: {
		path: path.resolve(__dirname, "public", "dist"),
		filename: "bundle.js"
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: [
							'@babel/preset-env',
							"@babel/preset-react",
						]
					}
				}
			},
			{
				test: /\.scss$/,
				exclude: /(node_modules|bower_components)/,
				use: [
					{
					loader: MiniCssExtractPlugin.loader,
					},
					"css-loader",
					"sass-loader",
			    ]
			}
        ]
	},
    plugins: [
    	new Dotenv(),
        new MiniCssExtractPlugin({
		  filename: 'bundle.css',
		  chunkFilename: '[id].css',
        })
    ]
};

module.exports = config;